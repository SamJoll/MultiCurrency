package main.ru.mncw.Commands;

import com.mojang.datafixers.kinds.IdF;
import main.ru.mncw.DataBases.PlayersDB;
import main.ru.mncw.DataBases.PlayersTXDB;
import main.ru.mncw.MultiCurrency;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class ShowTransactionsCommand implements CommandExecutor, Listener {

//    Класс плагина
    MultiCurrency plugin;
//    Класс БД игроков
    PlayersDB PlayersDB;
//    Класс БД транзакций
    PlayersTXDB PlayersTXDB;

//    Меню с транзакциями
    Inventory transactionsMenu;

    public ShowTransactionsCommand(MultiCurrency plugin) {
        this.plugin = plugin;

        transactionsMenu = plugin.transactionsMenu;

        try {
            PlayersDB = new PlayersDB();
            PlayersTXDB = new PlayersTXDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(CommandSender playerSender, Command comm, String label, String[] args) {
        if(playerSender instanceof Player) {
            try {
                if (args.length <= 0) {
//                    Ник игрока
                    final String playerName = playerSender.getName();
                    final int player_id = PlayersDB.GetPlayerID(Bukkit.getPlayer(playerName));

                    Connection dbCon = PlayersTXDB.getConnection();
                    Statement dbFlow = dbCon.createStatement();

                    ResultSet playerTransactionsRes = dbFlow.executeQuery("SELECT * FROM tx WHERE player_id = '" + player_id + "' ORDER BY DATETIME(date) DESC LIMIT " + 27);

                    for (int tI = 0; tI < 27; tI++) {
                        if(playerTransactionsRes.next()) {
                            ItemStack transactionPaper = new ItemStack(Material.PAPER);
                            ItemMeta transactionPaperInfo = transactionPaper.getItemMeta();

                            transactionPaperInfo.setDisplayName(plugin.getConfig().getString("transactions.datetime-color") + playerTransactionsRes.getString("date"));
                            transactionPaperInfo.setLore(Arrays.asList(new String[]{
                                    playerTransactionsRes.getString("description"),
                                    playerTransactionsRes.getString("status")
                            }.clone()));

                            transactionPaper.setItemMeta(transactionPaperInfo);
                            transactionsMenu.setItem(tI, transactionPaper);
                        } else {
                            break;
                        }
                    }

                    Bukkit.getPlayer(playerSender.getName()).openInventory(transactionsMenu);

                    dbFlow.close();
                    dbCon.close();

                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
}
