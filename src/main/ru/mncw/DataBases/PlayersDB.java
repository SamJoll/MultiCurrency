package main.ru.mncw.DataBases;

import org.bukkit.entity.Player;

import javax.swing.plaf.nimbus.State;
import java.sql.*;

public class PlayersDB {

//    Путь к файлу базы данных
    final String dbUrl = "jdbc:sqlite:plugins/MultiCurrency/players.db";

//    Конструктор класса
    public PlayersDB() {
        try {
            Class.forName("org.sqlite.JDBC").newInstance();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

            dbFlow.executeUpdate("CREATE TABLE IF NOT EXISTS players ('player_id' INTEGER PRIMARY KEY AUTOINCREMENT, 'playerName' TEXT, 'balance' DOUBLE, UNIQUE('playerName'))");

            dbFlow.close();
            dbCon.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    Соединение с БД
    Connection getConnection() throws Exception {
        return DriverManager.getConnection(dbUrl);
    }

//    Добавление нового игрока в БД
    public boolean AddNewPlayer(Player player) {
        try {
//            Ник нового игрока
            final String playerName = player.getName();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

            dbFlow.executeUpdate("INSERT OR IGNORE INTO players (playerName, balance) VALUES('"+ playerName +"', 0)");

            dbFlow.close();
            dbCon.close();

            return true;
        } catch (Exception e) {
            return false;
        }
    }
//    Вывод баланса игрока
    public double GetBalance(Player player) {
        try {
            final String playerName = player.getName();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

//            Баланс игрока
            ResultSet playerBalanceRes = dbFlow.executeQuery("SELECT balance FROM players WHERE playerName ='"+ playerName +"'");
            double playerBalance = playerBalanceRes.getDouble(1);

            dbFlow.close();
            dbCon.close();

            return playerBalance;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
//    Пополнение баланса
    public boolean AddMoney(Player player, double amount) {
        try {
//            Ник игрока
            final String playerName = player.getName();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

//            Текущий баланс игрока
            ResultSet playerRes = dbFlow.executeQuery("SELECT balance FROM players WHERE playerName = '"+ playerName +"'");
            double currentPlayerBalance = playerRes.getDouble(1);

            if(amount > 0) {
//                Новый баланс игрока
                double newPlayerBalance = currentPlayerBalance + amount;

                dbFlow.executeUpdate("UPDATE players SET balance ="+ newPlayerBalance +" WHERE playerName = '"+ playerName +"'");

                dbFlow.close();
                dbCon.close();

                return true;
            } else {
                dbFlow.close();
                dbCon.close();

                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
//    Снятие денег со счета
    public boolean SubtractMoney(Player player, double amount) {
        try {
//            Ник игрока
            final String playerName = player.getName();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

//            Текущий баланс игрока
            ResultSet playerBalanceRes = dbFlow.executeQuery("SELECT balance FROM players WHERE playerName = '"+ playerName +"'");
            double currentPlayerBalance = playerBalanceRes.getDouble(1);

            if(amount > 0 && currentPlayerBalance >= amount) {
                double newPlayerBalance = currentPlayerBalance - amount;

                dbFlow.executeUpdate("UPDATE players SET balance = "+ newPlayerBalance +" WHERE playerName ='"+ playerName +"'");

                dbFlow.close();
                dbCon.close();

                return true;
            } else {
                dbFlow.close();
                dbCon.close();

                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }
//    Перевод денег
    public boolean TransferMoney(Player player, Player targetPlayer, double amount) {
        try {
//            Ник отправителя
            final String playerName = player.getName();
//            Ник получателя
            final String targetPlayerName = targetPlayer.getName();

            if(playerName != targetPlayerName && SubtractMoney(player, amount) && AddMoney(targetPlayer, amount)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    Получения id игрока
    public int GetPlayerID(Player player) {
        try {
//            Ник игрока
            final String playerName = player.getName();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

            ResultSet playerRes = dbFlow.executeQuery("SELECT player_id FROM players WHERE playerName = '"+ playerName +"'");
            Integer playerId = playerRes.getInt(1);

            dbFlow.close();
            dbCon.close();

            return playerId;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
