package main.ru.mncw.DataBases;

import org.bukkit.entity.Player;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

public class PlayersTXDB {
//    Путь к файлу БД
    final String dbUrl = "jdbc:sqlite:plugins/MultiCurrency/playerstx.db";

//    Файл к БД игроков
    PlayersDB PlayersDB;

//    Конструктор класса
//    Создание экземпляров класса
    public PlayersTXDB() {
        try {
            PlayersDB = new PlayersDB();

            Class.forName("org.sqlite.JDBC").newInstance();

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

            dbFlow.executeUpdate("CREATE TABLE IF NOT EXISTS tx ('player_id' INTEGER, 'date' DATETIME, 'description' TEXT, 'status' TEXT)");

            dbFlow.close();
            dbCon.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    Подключение к БД
    public Connection getConnection() throws Exception {
        return DriverManager.getConnection(dbUrl);
    }

//    Запись транзакции
    public boolean WriteTransaction(Player player, String description, String status) {
        try {
//            ID игрока
            final Integer playerId = PlayersDB.GetPlayerID(player);

            Connection dbCon = getConnection();
            Statement dbFlow = dbCon.createStatement();

            dbFlow.executeUpdate(String.format("INSERT OR IGNORE INTO tx VALUES(%d, CURRENT_TIMESTAMP, '%s', '%s')", playerId, description, status));

            dbFlow.close();
            dbCon.close();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
