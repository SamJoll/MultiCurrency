package main.ru.mncw.Handlers;

import main.ru.mncw.MultiCurrency;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryHandler implements Listener {
//    Класс плагина
    MultiCurrency plugin;

//    Конструктор класса
    public InventoryHandler(MultiCurrency plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryClicked(InventoryClickEvent e) {
        if(e.getInventory().equals(plugin.transactionsMenu)) {
            e.setCancelled(true);
        }
    }
}
